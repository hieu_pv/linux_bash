#!/bin/bash

function clone {
    #load config data
    GIT_USER="$git_user"
    GIT_PASSWORD="$git_password"

    git_remote_ssh_pattern="^git(.*)*$"
    git_remote_http_pattern="^https(.*)*$"
    # strip slash at the end of string
    root_dir="$(echo $root_dir | sed 's/\/$//g')"

    #parse params
    while [[ $# -gt 1 ]]
    do
    key="$1"

    case $key in
        -t|--type)
        GIT_REPO_TYPE="$2"
        shift # past argument
        ;;
        -r|--remote)
        GIT_REMOTE_URL="$2"
        shift # past argument
        ;;
        -b|--branch)
        GIT_BRANCH="$2"
        shift # past argument
        ;;
        -n|--name)
        FOLDER_NAME="$2"
        shift # past argument
        ;;
        -u|--user)
        GIT_USER="$2"
        shift # past argument
        ;;
        -p|--password)
        GIT_PASSWORD="$2"
        shift # past argument
        ;;
        clone)
        # default function name
        ;;
        -f|--force)
        FORCE="YES"
        shift # past argument
        ;;
        --default)
        DEFAULT=YES
        ;;
        *)
        error "unknow option"
        ;;
    esac
    shift # past argument or value
    done

    # indicate the type of repository is set
    if [ -z ${GIT_REPO_TYPE+x} ]; then
        GIT_REPO_TYPE="ssh"
    fi

    if [[ $GIT_REPO_TYPE =~ $git_remote_ssh_pattern ]]; then
        GIT_REPO_TYPE="ssh"
    else 
        GIT_REPO_TYPE="http"
    fi

    # indicate the url of repository is set
    if [ -z ${GIT_REMOTE_URL+x} ]; then
        error "Please enter your repository url"
    fi

    # indicate the folder name is set
    if [ -z ${FOLDER_NAME+x} ]; then
        if [ "$GIT_REPO_TYPE" == "ssh" ]; then
            FOLDER_NAME="$(echo $GIT_REMOTE_URL | sed 's/\(.*\)\/\(.*\)/\2/g' | sed 's/.git//g')"
        else 
            FOLDER_NAME="$(echo $GIT_REMOTE_URL | sed 's/\(.*\)\/\(.*\)\/\(.*\)/\3/g' | sed 's/.git//g')"
        fi
    fi

    # check folder name is exist
    if [ -d $root_dir/$FOLDER_NAME ]; then 
        if [ "$FORCE" == "YES" ]; then 
            error "cannot create directory: File exists"
        else
            rm -rf $root_dir/$FOLDER_NAME
            echo $root_dir/$FOLDER_NAME "is removed successfully"
        fi
    fi

    # default branch that will be checked out after the repository cloned successfully
    if [ -z ${GIT_BRANCH+x} ]; then
        GIT_BRANCH="master"
    fi

    # create root directory if it not found 
    if ! [ -d $root_dir ]; then 
        mkdir $root_dir -p
        if [ $? -ne 0 ] ; then
            error "root directory not found"
        fi
    fi

    # change working directory to $root_dir
    cd $root_dir

    if [ "$GIT_REPO_TYPE" == "ssh" ]; then
        expect <<EOD
spawn git clone $GIT_REMOTE_URL $FOLDER_NAME
expect { 
    "*re you sure you want to continue connecting*" { send "yes\n"; exp_continue } 
}
EOD
    else
        expect <<EOD
spawn git clone $GIT_REMOTE_URL $FOLDER_NAME
expect { 
    "*sername*" { send "$GIT_USER\n"; exp_continue }
    "*assword*" { send "$GIT_PASSWORD\n"; exp_continue }
}
EOD
    fi
}

